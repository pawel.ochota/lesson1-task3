const { firstNumber, secondNumber, result } = require('..');

describe('Variable', () => {
  it('firstNumber should be a number', async () => {
    expect(typeof firstNumber).toBe('number');
  });

  it('secondNumber should be a number', async () => {
    expect(typeof secondNumber).toBe('number');
  });

  it('result should be a number', async () => {
    expect(typeof result).toBe('number');
  });
});

describe('Variable result', () => {
  it('should be the sum of firstNumber and secondNumber divided by 2', async () => {
    const finalValue = (firstNumber + secondNumber) / 2;
    expect(result).toBe(finalValue);
  });
});
